const validator = require('validator')

exports.isValideLength = (word, min, max) => validator.isLength(word, { min: min, max: max })
exports.isAbsent = (thing) => validator.isEmpty(thing)

